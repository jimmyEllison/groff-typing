.TL
MOT SO VAN DE LY LUAN VA THUC TIEN VE CHU NGHIA XA HOI VA CON DUONG DI LEN CHU
NGHIA XA HOI O VIET NAM
.AU
GS TS Nguyen Phu Trong
.AI
NXB Chinh tri quoc gia Su that
.PP
.I "Chu nghia xa hoi va con duong di len chu nghia xa hoi o Viet Nam"
la mot de tai ly luan va thuc tien rat co ban, quan trong, noi dung rat rong
lon, phong phu va phuc tap, co nhieu cach tiep can khac nhau, doi hoi phai co su
nghien rat cong phu, nghiem tuc, tong ket thuc tien mot cach sau sac, khoa hoc.
Trong pham vi bai biet nay, toi chi xin de cap mot so khia canh tu goc nhin thuc
tien cua Viet Nam. Va cung chi tap trung va tra loi may cau hoi: Chu nghia xa
hoi la gi?  Vi sao Viet Nam lua chon con duong xa hoi chu nghia? Lam the nao va
bang cach nao de tung buoc xay dung duoc chu nghia xa hoi o Viet Nam? Thuc tien
cong cuoc doi moi, di len chu nghia xa hoi o Viet Nam trong thoi gian qua co y
nghia gi va dat ra van de gi?
.PP
Nhu chung ta da biet, chu nghia xa hoi thuong duoc hieu voi ba tu cach: Chu
nghia xa hoi la mot
.I "hoc thuyet" ";"
chu nghia xa hoi la mot
.I "phong trao" ";"
chu nghia xa hoi la mot
.I "che do" "."
Moi tu cach ay lai co nhieu bieu hien khac nhau, tuy theo the gioi quan va trinh
do phat trien o moi giai doan lich su cu the.
.FS "*"
Bai viet nhan ky niem 131 nam Ngay sinh Chu tich Ho Chi Minh (19/5/1890 -
19/5/2021), bau cu dai bieu Quoc hoi khoa XV va dai bieu Hoi dong nhan dan cac
cap nhiem ky 2021 - 2026, ngay 15/5/2021.
.FE
.bp
.LP
Chu nghia xa hoi de cap o day la chu nghia xa hoi khoa hoc dua tren hoc thuyet
Mac - Lenin trong thoi dai ngay nay. Vay thi chung ta phai dinh hinh chu nghia
xa hoi the nao va dinh huong di len chu nghia xa hoi the nao cho phu hop voi
hoan canh, dac diem cu the o Viet Nam?
.PP
Truoc day, khi con Lien Xo va he thong cac nuoc xa hoi chu nghia the gioi thi
van de di len chu nghia xa hoi o Viet Nam duong nhu khong co gi phai ban, no
mac nhien coi nhu da duoc khang dinh. Nhung tu sau khi mo hinh chu nghia xa hoi
o Lien Xo va nhieu nuoc Dong Au sup do, cach mang the gioi lam vao thoai trao
thi van de di le chu nghia xa hoi lai duoc dat ra va tro thanh tam diem thu hut
moi su ban thao, tham chi tranh luan gay gat. Cac the luc chong cong, co hoi
chinh tri hi hung, vui mung, thua co dan toi de xuyen tac, chong pha.  Trong
hang ngu cach mang cung co nguoi bi quan, dao dong, nghi ngo tinh dung dan, khoa
hoc cua chu nghia xa hoi, quy ket nguyen nhan tan ra cua Lien Xo va mot so nuoc
xa hoi chu nghia Dong Au la do sai lam cua chu nghia Mac - Lenin va su lua chon
con duong xay dung chu nghia xa hoi. Tu do ho cho rang chung ta da chon sai
duong, can phai di con duong khac. Co nguoi con phu hoa voi cac luan dieu thu
dich cong kich, bi cac chu nghia xa hoi, ca ngoi mot chieu chu nghia tu ban.
Tham chi co nguoi con sam hoi ve mot thoi da tin theo chu nghia Mac - Lenin va
con duong xa hoi chu nghia! Thuc te co phai nhu vay khong? Thuc the co phai hien
nay chu nghia tu ban, ke ca nhung nuoc tu ban chu nghia gia doi van dang phat
trien tot dep khong? Co phai viet Nam chung da chon con duong di sai khong?
.PP
Chung ta thua nhan rang, chu nghia tu ban chua bao gio mang tinh toan cau nhu
ngay nay va cung da dat duoc nhieu thanh tuu to lon, nhat la trong linh vu giai
phong va phat trien
.bp
.LP
suc san xuat, phat trien khoa hoc va cong nghe. Nhieu nuoc tu ban phat trien,
tren co so cac dieu kien kinh te cao va do ket qua dau tran cua giai cap cong
nhan va nhan dan lao dong, da co nhung bien phap dieu chinh, hinh thanh duoc
khong it cac che do phuc loi xa hoi tien bo hon so voi truoc. Tu giua thap ky 70
cua the ky XX va nhat tu sau khi Lien Xo tan ra, de thich ung voi dieu kien moi,
chu nghia tu ban the gioi da ra suc tu dieu chinh, thuc day cac chinh sach \*Qtu
do moi\*U tren quy mo toan cau, nho do hien van con tiem nang phat trien.  Tuy
nhien, chu nghia tu ban van khong the khac duoc nhung mau thuan co ban von co
cua no. Cac cuoc khung hoang van tiep tuc dien ra. Dac biet, nam 2008 - 2009,
chung ta da chung kien cuoc khung hoang tai chinh, suy thoai kinh te bat dau tu
nuoc My, nhanh chong lan rong ra cac trung tam tu ban chu nghia khac va tac dong
den hau het cac nuoc tren the gioi. Cac nha nuoc, cac hinh phu tu san o phuong
Tay da bom nhung luong tien khong lo  de cuu cac tap doan kinh te xuyen quoc
gia, cac to hop cong nghiep, tai chinh, ngan hang, thi truong chung khoan, nhung
khong may thanh cong. Va hom nay , chung ta lai chung kien cuoc khung hoang nhie
u mat , ca ve y te, xa hoi lan chinh tri, kinh te dang dien ra duoi tac dong cua
dai dich Covid-19 va cuoc Cach mang cong nghiep lan thu tu.  Kinh te suy thoai
da lam phoi bay su that cua nhung bat cong xa hoi trong cac xa hoi tu ban chu
nghia: Doi song cua da so dan cu lao dong bi giam sut nghiem trong, that nghiep
gia tang; khoang cach giau - ngheo ngay cang lon, lam tram trong them nhhung mau
thuan, xung dot giua cac sac toc. Nhung tinh huong \*Qphat trien xau\*U, nhung
nghich ly \*Qphan phat trien\*U, tu dia hat kinh te - tai chinh da tran sang
linh vuc xa hoi lam bung no cac xung dot xa hoi va o khong it noi tu tinh uong
kinh te da tro thanh tinh huong chinh tri voi cac 
.bp
.LP
lan song bieu tinh, bai cong, lam rung chuyen ca the che. Su that cho that, ban
than thi truong tu do cua chu nghia tu ban khong the giup giai quyet duoc nhung
kho khan, va trong nhieu truong hop con gay ra nhung ton hai nghiem trong cho
cac nuoc ngheo; lam sau sac them mau thuan giua
.I "lao dong" " "
va
.I "tu ban" " "
toan cau. Su that do cung lam pha san nhung ly thuyet kinh te hay mo hinh phat
trien von xua nay duoc coi la thoi thuong, duoc khong it cac chinh khach tu san
ca ngoi, duoc cac chuyen gia cua ho coi la toi uu, hop ly.
.PP
Cung voi khung hoang kinh te - tai chinh la khung hoang nang luong, luong thuc,
su can kiet cua cac nguon tai nguyen thien nhien, su su thoai cua moi truong
sinh thai,... dang dat ra nhung thach thuc vo cung lon cho su ton tai va phat
trien cua nhan loai. Do la hau qua cua mot qua trinh phat trien kinh te - xa hoi
lau loi nhuan lam muc tieu toi thuong, coi chiem huu cua cai va tieu dung vat
chat ngay cang tang lam cho thuoc van minh, lay loi ich ca nhan lam tru cot xa
hoi. Do cung chinh la nhung dac trung cot yeu cua phuong thuc san xuat va tieu
dung tu ban chu nghia. Cac cuoc khung hoang dang dien ra mot lan nua chung minh
tinh
.I "khong ben vung" " " 
ca ve 
.I "kinh te" ","
.I "xa hoi" "," 
va 
.I "moi truong sinh thai" " "
cua no. Theo nhieu nha khoa hoc phan tich, cac cuoc khung hoang hien nay khong
the giau quyet duoc mot cac triet de trong khuon kho cua che do tu ban chu
nghia.
.PP
Cac phong trao phan khang xa hoi bung no manh me tai nhieu nuoc tu ban phat
trien trong thoi gian qua cang lam boc lo ro su that ve ban chat cuac the che
chinh tri tu ban chu nghia. Thuc te la cac thiet che dan chu theo cong thuc \*Q
dan chu tu do\*U ma phuong Tay ra suc quang ba, ap dat len toan the gioi khong
he bao dam de quyen luc thuc su thuoc ve nhan dan, do nhan dan va vi nhan dan -
yeu to ban chat nhat cua dan chu.
.bp
.LP
He thong quyen luc do van chu yeu thuoc ve thieu so giau co va phuc vu loi ich
cua cac tap doan tu ban lon. Mot bo phan rat nho, tham chi chi la 1% dan so,
nhung lai chiem giu phan lon cua cai, tu lieu san xuat, kiem soat toi 3/4 nguon
tai chinh, tri thuc va cac phuong tien thong tin dai chung chu yeu va do do chi
phoi toan xa hoi. Day chinh la nguyen nhan sau xa dan den phong trao \*Q99 chong
lai 1\*U dien ra o My dau nam 2011 va nhanh chong lan rong o nhieu nuoc tu ban.
Su reu rao binh dang ve quyen, nhung khong kem theo su binh dang ve dieu kien de
thuc hien cac quyen do da dan den dan chu van chi la hinh thuc, trong rong ma
khong thuc chat. Trong doi song chinh tri, mot khi quyen luc cua dong tien chi
phoi thi quyen luc cua nhan dan se bi lan at. Vi vay ma tai cac nuoc tu ban phat
trien , cac cuoc bau cu duoc goi la \*Qtu do\*U, \*Qdan chu\*U, du co the thay
doi chinh phu, nhung khong the thay doi duoc cac the luc thong tri; dang sau he
thong da dang tren thuc te van la su chuyen che cua cac tap doan tu ba, du co
the thay doi chinh phu, nhung khong the thay doi duoc cac the luc thong tri;
dang sau he thong da dang tren thuc te van la su chuyen che cua cac tap doan tu
ban.
.PP
Chung ta can mot xa hoi ma trong su phat trien la thuc su vi con nguoi, chu
khong phai vi loi nhuan ma boc lot va cha dap len pham gia con nguoi. Chung ta
can su phat trien ve kinh te di doi voi tien va cong bang xa hoi, chu khong phai
gia tang khoang cach giau ngheo va bat binh xa hoi. Chung ta can mot xa hoi nhan
ai, doan ket, tuong tro lan nhau, huong toi cac gia tri tien bo, nhan van, chu
khong phai canh tranh bat cong, \*Qca lon nuot ca be\*U, vi loi ich vi ly cua
mot so it ca nhan va cac phe nhom. Chung ta can su paht trien ben vung, hai hoa
voi thien nhien de bao dam moi truonjg song trong lanh cho cac the he hien tai
va tuong lai, chu khong phai khai thac, chiem doat tai nguyen, tieu dung vat
chat vo han do va huy hoai moi truong. Va, chung ta can mot he thong chinh tri
ma quyen luc thuc su thuoc ve nhan dan, do nhan dan va phuc vu loi ich cua nhan
dan,
.bp
.LP
chu khong phai chi cho mot thieu so giau co. Phai chang nhung mong uoc tot dep
do chinh nhung gia tri dich thuc cua chu nghia xa hoi va cung chinh la muc tieu,
la con duong ma Chu tich Ho Chi Minh, Dang ta va nhan dan ta da lua chon va dang
kien dinh, kien tri theo duoi.
.PP
Nhu chung ta deu biet, nhan dan Viet Nam da trai qua mot qua trinh dau tranh
cach mang lau dai, kho khan, day gian kho, hy sinh de chong lai ach do ho va su
xam luoc cua thuc dan, de quoc de bao ve nen doc lap dan toc va chu quyen thieng
lieng cua dat nuoc, vi tu do, hanh phuc cua nhan dan voi tinh than
.I "Khong co gi quy hon doc lap, tu do" "\*U." "\*Q"
.PP
Doc lap dan toc gan lien voi chu nghia xa hoi la duong loi co ban, xuyen suot
cua cach mang Viet Nam va cung la diem cot yeu trong di san tu tuong cua Chu
tich Ho Chi Minh. Bang kinh nghiem thuc tien phong phu cua minh ket hop voi ly
luan cach mang, khoa hoc cua chu nghia Mac - Lenin, Ho Chi Minh da dua ra ket
luan sau sac rang,
.I "chi co chu ngia xa hoi va chu nghia cong san moi co the giai quyet triet de \
van de doc lap cho dan toc, moi co the dem lai cuoc song tu do, am no va hanh \
phuc thuc su cho tat ca moi nguoi, cho cac dan toc" "."
.PP
Ngay khi moi ra doi va trong suot qua trinh dau tranh cach mang, Dang Cong san
Viet Nam luon khang dinh: Chu nghia xa hoi la muc tieu, ly tuong cua Dang Cong
san va nhan dan Viet Nam; di len chu nghia xa hoi la yeu cau khach quan, la con
duong tat yeu cua cach mang Viet Nam. Nam 1930, trong Cuong linh chinh tri cua
minh, Dang Cong san Viet Nam da chu truong tien hanh cach mang dan toc dan chu
nhan dan do giai cap cong nhan lanh dao, \*Qbo qua thoi tu bon chu nghia ma
tranh dau
.bp
.LP
thang len con duong xa hoi chu nghia\*U\**. Vao nhung nam cuoi the ky XX, mac du
tren the gioi, chu nghia xa hoi hien thuc da bi do vo mot mang lon, he thong cac
nuoc xa hoi chu nghia da khong con, phong trao xa hoi chu nghia lam vao giai
doang khung hoang, thoai trao, gap rat nhieu kho khan, Dang Cong san Viet Nam
van tiep tuc khang dinh:
.I "Dang va nhan dan ta quyet tam xay dung dat nuoc Viet Nam theo con duong \
xa hoi chu nghia tren nen tang chu nghia Mac - Lenin va \
tu tuong Ho Chi Minh" "\*U\**." "\*Q"
Tai Dai hoi dai bieu toan quoc lan thu XI cua Dang (thang 01/2011), trong
.I "Cuong linh xay dung dat nuoc trong thoi ky qua do len chu nghia xa hoi"
(bo sung, phat trien nam 2011), chung ta mot lan nua khang dinh:
.I "Di len xa hoi chu nghia la khat vong cua nhan dan ta, la su lua chon dung \
dan cua Dang Cong san Viet Nam va Chu tich Ho Chi Minh, phu hop voi xu the phat \
trien cua lich su" "\*U\**." "\*Q"
.PP
Tuy nhien, chu nghia xa hoi la gi va di len chu nghia xa hoi bang cach nao? Do
la dieu ma chung luon luon tran tro, suy nghi, tim toi, lua chon de tung buoc
hoan thien duong loi, quan diem va to chuc thuc hien, lam sao de vua theo dung
quy luat chung, vua phu hop voi dieu kien cu the o Viet Nam.
.PP
Trong nhung nam tien hanh cong cuoc doi moi, tu tong ket thuc tien va nghien cuu
ly luan, Dang Cong san Viet Nam tung buoc nhan thuc ngay cang dung dan hon, sau
sac hon ve chu nghia xa hoi va thoi ky qua do len chu nghia xa hoi; tung buoc
khac phuc mot so quan niem don gian truoc day nhu: dong nhat
.nr FF 1
.FS
Dang Cong san Viet Nam: Van kien Dang toan tap, Nxb. Chinh tri quoc gia, Ha Noi,
2002, t.2, tr.94 (B.T).
.FE
.FS
Dang Cong san Viet Nam: Van kien Dang toan tap, Sdd, t.60, tr.78 (B.T).
.FE
.FS
Dang Cong san Viet Nam: Van kien Dai hoi dai bieu toan quoc lan thu XI, Nxb.
Chinh tri quoc gia, Ha Noi, 2011, tr.70 (B.T).
.FE
.bp
.LP
muc tieu cuoi cung cua chu nghia xa hoi voi nhiem vu cua giai doan truoc mat;
nhan manh mot chieu quan he san xuat, che do phan phoi binh quan, khong thay day
du yeu cau phat trien luc luong san xuat trong thoi ky qua do, khong thua nhan
su ton tai cua cac thanh phan kinh te; dong nhat kinh te thi truong voi chu
nghia tu ban; dong nhat nha nuoc phap quyen voi nha nuoc tu san...
.PP
Cho den nay, mac du van con mot so van de can tiep tuc di sau nghien cuu, nhung
chung ta da hinh thanh nhan thuc tong quat: Xa hoi xa hoi chu nghia ma nhan dan
Viet Nam dang phan dau xay dung la mot xa hoi dan giau, nuoc manh, dan chu, cong
bang, van minh; do nhan dan lam chu; co nen kinh te phat trien cao, dua tren luc
luong san xuat hien dai va quan he san xuat tien bo phu hop; co nen van hoa tien
tien, dam da ban sac dan toc; con nguoi co cuoc song am no, tu do, hanh phuc, co
dieu kien phat trien toan dien; cac dan toc trong cong dong Viet Nam binh dang,
doan ket, ton trong va giup do nhau cung phat trien; co Nha nuoc phap quyen xa
hoi chu nghia cua nhan dan, do nhan dan, vi nhan dan do Dang Cong san lanh dao;
co quan he huu nghi va hop tac voi cac nuoc tren the gioi.
.PP
De thuc hien duoc muc tieu do, chung ta phai: Day manh cong nghiep hoa, hien dai
hoa dat nuoc gan voi phat trien kinh te tri thuc; phat trien nen kinh te thi
truong dinh duong xa hoi chu nghia; xay dung nen van hoa tien tien, dam da ban
sac dan toc, xay dung con nguoi, nang cao doi song nhan dan, thuc tien tien bo
cong ban xa hoi; bao dam vung chac quoc phong va an ninh quoc gia, trat tu, an
toan xa hoi; thuc hie nduong loi doi ngoai doc lap, tu chu, da phuong hoa, da
dan hoa, hoa binh, huu nghi, hop tac va phat trien, chu dong va tich cuc hoi
nhap quoc te; xay dung nen dan chu xa hoi chu nghia, phat huy y chi va y chi va
.bp
.LP
suc manh dai doan ket dan toc, ket hop voi suc manh thoi dai; xay dung Nha nuoc
phap quyen xa hoi chu nghia cua nhan dan, do nhan dan, vi nhan dan; xay dung
Dang va he thong chinh tri trong sach, vung manh toan dien.
.PP
Cang di vao chi dao thuc tien, Dang ta cang nhan thuc duoc rang, qua do len chu
nghia xa hoi la mot su nghiep lau dai, vo cung kho khan va phuc tap, vi no phai
tao su bien doi sau sac ve chat tren tat ca cac linh vuc cua doi song xa hoi.
Viet Nam di len chu nghia xa hoi tu mot nuoc nong nghiep lac hau, bo qua che do
tu ban chu nghia, luc luong san xuat rat thap, lai trai qua may chuc nam chien
tranh, hau qua rat nang nel cac the luc thu dich thuong xuyen tim cach pha hoai
cho nen lai cang kho khan, phuc tap, nhat thiet phai trai qua mot thoi ky qua do
lau dai voi nhieu buoc di, nhieu hinh thuc to chuc kinh te, xa hoi dan xen nhau,
co su dau tranh giua cai cu va cai moi. Noi bo qua che do tu ban chu nghia la bo
qua che do ap buc, bat cong, boc lot tu ban chu nghia; bo qua nhung thoi hu tat
xau, nhung thiet che, the the chinh tri khong phu hop voi che do xa hoi chu
nghia, chu khong phai bo qua ca nhung thanh tuu, gia tri van minh ma nhan loai
da dat duoc trong thoi ky phat trien chu nghia tu ban. Duong nhien, viec ke thua
nhung thanh tuu nay phai co chon loc tren quan diem khoa hoc, phat trien.
.PP
Dua ra quan niem phat trien kinh te thi truong dinh huong xa hoi chu cnghia la
mot dot pha ly luan rat co ban va sang tao cua Dang ta, la thanh qua ly luan
quan trong qua 35 nam thuc hien duong loi doi moi, xuat phat tu thuc tien Viet
Nam va tiep thu co chon loc kinh nghiem cua the gioi. Theo nhan thuc cua chung
ta, kinh te thi truong dinh huong xa hoi chu nghia la nen kinh te thi truong
hien dai, hoi nhap quoc te, van hanh day du, dong bo theo cac quy luat cua kinh
te thi truong, co su quan ly cua nha nuoc
.bp
.LP
phap quyen xa hoi chu nghia, do Dang Cong san Viet Nam lanh dao; bao dam dinh
huong xa hoi chu nghia, nham muc tieu \*Qdan giau, nuoc manh, dan chu, cong
bang, van minh\*U. Do la mot kieu kinh te thi truong moi trong lich su phat
trien kinh te thi truong; mot kieu to chuc kinh te vua tuan theo nhung quy luat
cua kinh te thi truong, vua tren co so va duoc dan dat, chi phoi boi cac nguyen
tac va ban chat cua chu nghia xa hoi, the hien tren ca ba mat: so huu, to chuc
quan ly va phan phoi. Day khong phai la nen kinh te thi truong tu ban chu nghia
va cung chua phai la nen kinh te thi truong xa hoi chu nghia day du(vi nuoc ta
con dang trong thoi ky qua do).
.PP
Trong nen kinh te thi truong dinh huong xa hoi chu nghia co nhieu hinh thuc so
huu, nhieu thanh phan kinh te. Cac thanh phan kinh te hoat dong theo phap luat
deu la bo phan hop thanh quan trong cua nen kinh te, binh dang truoc phap luat
cung phat trien lau dai, hop tac va canh tranh lanh manh. Trong do, kinh te nha
nuoc giu vai tro chu dao; kinh te tap the, kinh te tu nhan la mot dong luc quan
trong cua nen kinh te; kinh te co von dau tu nuoc ngoai duoc khuyen khich phat
trien phu hop voi chien luoc, quy hoach phat trien kinh te - xa hoi. Quan he
phan phoi bao dam cong bang va tao dong luc cho phat trien; thuc hien che do
phan phoi chu yeu theo ket qua lao dong, hieu qua kinh te, dong thoi theo muc
dong gop von cung cac nguon luc khac va phan phoi thong he thong an sinh xa hoi,
phuc loi xa hoi. Nha nuoc quan ly nen kinh te bang phap luat, chien luoc, quy
hoach, ke hoach, chinh sach va luc luong vat chat de dinh huong, dieu tiet, thuc
day phat trien kinh te - xa hoi.
.PP
Mot dac trung co ban, mot thuoc tinh quan trong cua dinh huong xa hoi chu nghia
trong kinh te thi truong o Viet Nam la
.bp
.LP
phai gan kinh te voi xa hoi, thong nhat chinh sach kinh te voi chinh sach xa
hoi, tang truong kinh te di doi voi thuc hien tien bo va cong bang xa hoi ngay
trong tung buoc, tung chinh sach va trong suot qua trinh phat trien. Dieu do co
nghia la: khong cho den khi kinh te dat toi trinh do phat trien cao roi moi thuc
hien tien bo va cong bang xa hoi, cang khong \*Qhy sinh\*U tien bo va cong bang
xa hoi de chay the tang truong kinh te don thuan. Trai lai, moi chinh sach kinh
te deu phai huong toi muc tieu phat trien xa hoi; moi chinh sach xa hoi phai
nham tao ra dong luc thuc day phat trien kinh te; khuyen khich lam giau hop phap
phai di doi voi xoa doi, giam ngheo ben vung, cham soc nhung nguoi co cong,
nhung nguoi co hoan canh kho khan. Day la mot yeu cau co tinh nguyen tac de bao
dam su phat trien lanh manh, ben vung, theo dinh huong xa hoi chu nghia.
.PP
Chung ta coi van hoa la nen tang tinh tan cua xa hoi, suc manh noi sinh, dong
luc phat trien dat nuoc va bao ve To quoc; xac dinh phat trien van hoa dong bo,
hai hoa voi tang truong kinh te va tien bo, cong bang xa hoi la mot dinh huong
can ban cua qua trinh xay dung chu nghia xa hoi o Viet Nam, Nen van hoa ma chung
ta xay dung la nen van hoa tien tien dam da ban sac dan toc, mot nen van hoa
thong nhat trong da dang, dau tren cac gia tri tien bom nhan van; chu nghia Mac
-Lenin va tu tuong Ho Chi Minh giu vai tro chu dao trong doi song tinh than xa
hoi, ke thua va phat huy nhung gia tri truyen thong tot dep cua tat ca cac dan
toc trong nuoc, tiep thu nhung thanh tuu, tinh hoa van hoa nhan loai, phan dau
xay dung mot xa hoi van minh, lanh manh, vi loi ich chan chinh va pham gia con
nguoi, voi trinh do tri thuc, dao duc, the luc, loi song va tham my ngay cang
cao. Chung ta xac dinh: Con nguoi giu iv tri trung tam trong chien luoc phat
trien; phat trien van hoa, xay dung con nguoi vua la muc tieu, vua la
.bp
.LP
dong luc cua cong cuoc doi moi; phat trien giao duc - dao tao va khoa hoc va
cong nghe la quoc sach hang dau bao ve moi truong la mot trong nhung van de song
con, la tieu chi de phat trien ben vung; xay dung gia dinh hanh phuc tien bo
lam te bao lanh manh, vung chac cua xa hoi, thuc hien binh dang gioi la tieu chu
cua tien bo, van minh.
